/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ESTUDIANTE
 */
public class Line extends Shape {
    Point end;

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public Line( Point start,Point end) {
        super(start);
        this.end = end;
    }
    
    public int perimeter()
    {
        return end.getX()-start.getX();
    }
    
}
