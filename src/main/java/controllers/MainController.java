/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import java.util.ArrayList;
import models.Line;
import models.Point;
import views.MainWindow;

/**
 *
 * @author ESTUDIANTE
 */
public class MainController {
    ArrayList <Line> lines;
    MainWindow window;
    public MainController() {
       lines = new ArrayList();
       window=new MainWindow();
       for (int i=0;i<10;i++)
       {
          int x1=(int)(500*Math.random());
          int y1=(int)(500*Math.random());
          int x2=(int)(500*Math.random());
          int y2=(int)(500*Math.random());
          Line line=new Line(new Point(x1,y1),new Point(x2,y2));
          lines.add(line);
       }
    }
    public void start()
    {
        window.setPanel(lines);
        window.setVisible(true);
    }
    
    
}
